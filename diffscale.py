"""
Usage:
    diffscale auto <scale> <name>
    diffscale custom <name>

Patches all the level files with scaled spawn rates
"""
from typing import Dict, Generator, List, Optional, Set
import os
import re
import sys

from docopt import docopt


def walk_files(path: str) -> Generator[str, None, None]:
    for root, dirs, files in os.walk(path):
        for file in files:
            yield os.path.join(root, file)

def read_lists(basedir: str) -> Dict[str, Set[str]]:
    result = {}

    for file in walk_files(os.path.join(basedir, 'lists')):
        name = file.rsplit('/', 1)[1].rsplit('.', 1)[0]
        result[name] = set(open(file, 'r').read().splitlines())

    return result

def file_patch(file: str, file_new: str, lists: Dict[str, Set[str]], scale: Dict[str, int]) -> None:
    with open(file, 'r') as f:
        lines = f.read().splitlines()

    for i, line in enumerate(lines):

        if (m := re.match(r'\\[%\+](\w+)\s+(.+)', line)):
            string = m.group(1)
            remainder = m.group(2)

            for name in lists:
                if string not in lists[name]:
                    continue

                if name not in scale or scale[name] <= 1:
                    continue

                values_str = re.match(r'(\d+(, \d+)*)(\s+.+)?', remainder).group(1)
                values = [max(1, int(i) // scale[name]) for i in values_str.split(', ')]
                lines[i] = line.replace(values_str, str(values)[1:-1])
                break

    os.makedirs(os.path.dirname(file_new), exist_ok=True)

    with open(file_new, 'w') as f:
        f.write('\n'.join(lines))

def scale_dict(value: Optional[int]) -> Dict[str, int]:
    if value:
        scale = {
            'monsters': value,
            'traps': value,
            'critters': 1,
        }
    else:
        scale = {
            'monsters': int(input('Enter monster spawn scaling: ')),
            'traps': int(input('Enter traps spawn scaling: ')),
            'critters': 1,
        }

    return scale

def generate(scale: Dict[str, int], name: str) -> str:
    basedir = os.path.dirname(os.path.abspath(__file__))
    lists = read_lists(basedir)

    vanilla = os.path.join(basedir, 'vanilla')
    result = os.path.join(basedir, name)

    for file in walk_files(vanilla):
        if file.endswith('.lvl'):
            file_new = file.replace(vanilla, result, 1)
            file_patch(file, file_new, lists, scale)

    return result

def main():
    args = docopt(__doc__)
    scale = scale_dict(int(args['<scale>']) if args['<scale>'] else None)
    print(generate(scale, args['<name>']))

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('\nInterrupted')
        sys.exit(130)