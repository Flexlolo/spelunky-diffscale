import os
import tarfile

from diffscale import generate, scale_dict


basedir = os.path.dirname(os.path.abspath(__file__))
release = os.path.join(basedir, 'release')
os.makedirs(release, exist_ok=True)
os.chdir(release)

for i in (2, 3, 4, 5, 8, 10, 16):
	scale = scale_dict(i)
	target = generate(scale, f'release/x{i}')

	try:
		tar = tarfile.open(target + '.tar.xz', 'x:xz')
		tar.add(target.rsplit('/', 1)[1])
		tar.close()
	except FileExistsError:
		pass