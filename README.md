# spelunky-diffscale
Tool to modify monster and trap spawn rates

## Dependencies
```
docopt
```

## Usage
```
python diffscale.py SCALE
```

## Planned features
* Shuffle monsters/traps
* <INSERT YOUR MONSTER/TRAP HERE> everywhere generator
